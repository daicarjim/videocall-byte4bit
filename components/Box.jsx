import React from "react";
import { useLoader } from "@react-three/fiber";
import { TextureLoader } from "three";

function Box(props) {
    const texture = useLoader(TextureLoader, "/byte4.png");
  return (
    <mesh {...props} recieveShadow={true} castShadow={true}>
      <boxBufferGeometry />
      <meshPhysicalMaterial  map={texture} color={"white"} />
    </mesh>
  );
}
export default Box;
