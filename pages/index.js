/*global JitsiMeetExternalAPI*/
import { Canvas } from "@react-three/fiber";
import React, { useState, useEffect } from "react";
import { Container, Form, Button } from "react-bootstrap";
import swal from "sweetalert";
import css from "../styles/Home.module.css";
import Floor from "../components/Floor";
import Box from "../components/Box";
import LightBulb from "../components/LightBulb";
import OrbitControls from "../components/OrbitControls";
import Draggable from "../components/Draggable";
import { Suspense } from "react";

function App() {
  // Setup our Hooks to manage state
  const [email, setEmail] = useState("");
  const [allowed, setAllowed] = useState(false);

  // We're using React Hooks to load the JaaS iFrame API
  useEffect(() => {
    const script = document.createElement("script");
    script.src = "https://8x8.vc/external_api.js";
    script.async = true;
    document.body.appendChild(script);
    return () => {
      document.body.removeChild(script);
    };
  }, []);

  // Init the JaaS iFrame API once we have a JWT
  const initJaas = (data) => {
    new JitsiMeetExternalAPI("8x8.vc", {
      roomName: data.room,
      parentNode: document.querySelector("#jaas-container"),
      jwt: data.key,
    });
  };

  // Handle the user submission
  const onSubmit = async (e) => {
    await e.preventDefault();

    const res = await fetch("/api/join", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email: email }),
    });

    const data = await res.json();

    if (data.success) {
      setAllowed(data.success);
      setTimeout(() => {
        initJaas(data);
      }, 500);
    } else {
      swal("Acceso denegado!", "Por favor verifica tu correo", "error");
    }
  };

  return (
    <div className="App">
      <Container>
      <h2>Bienvenido</h2> 
        <Form
          onSubmit={onSubmit}
          style={{ display: allowed ? "none" : "block" }}
        >
          <div className={css.scene}>
          <Canvas
            shadows={true}
            className={css.canvas}
            camera={{
              position: [-1, 2, 2],
            }}
          >
            <ambientLight color={"white"} intensity={0.5} />
            <LightBulb position={[0, 3, 0]} />
            <Draggable>
              <Suspense fallback={null}>
                <Box rotateX={3} rotateY={0.2} />
              </Suspense>
            </Draggable>
            <OrbitControls />
            <Floor position={[0, -1, 0]} />
          </Canvas>
        </div>
          <Form.Group>
            <Form.Label as="legend" htmlFor="email">Correo Electrónico</Form.Label>
            <Form.Control
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Form.Group>
          <Button variant="dark" type="submit">
            Ingresar
          </Button>
        </Form>
        
        <div style={{ display: allowed ? "block" : "none" }}>
          <div id="jaas-container" style={{ height: "700px" }}></div>
        </div>
      </Container>
    </div>
  );
}

export default App;
